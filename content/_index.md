+++
[banner]

  [[banner.button]]
      url = "#feature-icons"
      text = "Find out more"

#Details for the box below the banner
# https://www.openstreetmap.org/node/3927481011

# [Marienplatz](https://www.openstreetmap.org/node/2758329662)
# [Harras](https://www.openstreetmap.org/node/3927481011)
# [Rotkreuzplatz](https://www.openstreetmap.org/way/10299265)
# [Stachus](https://www.openstreetmap.org/node/337854210)

[services]
  title = "Laßt uns reden!"
  text = "Jeden Freitag Debattenkultur im öffentlichen Raum von 17-19 Uhr <br/>Am 16.4.2021 auf dem [Rotkreuzplatz](https://www.openstreetmap.org/way/10299265)."
  map_location = "München"

[feature_icons]
  #These feature icons look best if there's an even number of them.
  enable = true

  #Accent is a colour defined in the CSS file. Choose between 1 and 5    
  [[feature_icons.tile]]
    icon = "fa-comments"
    icon_pack = "fas"
    accent = "1"
    title = "Offener Diskurs"
    text = "Miteinander reden, statt vorurteilen."
#    url = "/offener-diskurs"

  [[feature_icons.tile]]
    icon = "fa-calendar-alt"
    icon_pack = "fas"
    accent = "3"
    title = "Termine"
    text = "Die nächsten Termine in München."
    url = "/dates"

  [[feature_icons.tile]]
    icon = "fa-filter"
    icon_pack = "fas"
    accent = "2"
    title = "Filterblase aus"
    text = "Im öffentlichen Raum die Filterblase digitaler Netzwerke verlassen."
#    url = "/filterblase-aus"

  [[feature_icons.tile]]
    icon = "fa-hand-peace"
    icon_pack = "fas"
    accent = "5"
    title = "Friedlicher Austausch"
    text = "Kein Haß, kein Extremismus, keine Gewalt."
#    url = "/friedlicher-austausch"

[feature_images]
#These feature images look best if there's an even number of them.
  enable = false

  [[feature_images.tile]]
#    image = "img/freely-10057.jpg"
    title = "Childrens' Ministry"
    text = "A church for the family."
    url = "/kids"
    button_text = "Learn more"

  [[feature_images.tile]]
#    image = "img/freely-26905.jpg"
    title = "Midweek Ministries"
    text = "Want more during the week?"
    url = "#"
    button_text="Join a group today!"

+++
