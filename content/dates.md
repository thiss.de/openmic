---
title: "Termine in München"
---

- 2.4.2021 17-19
  Open Mic am Harras
- 9.4.2021 17-19
  Open Mic / Rotkreuzplatz.
- 16.4.2021 17-19
  Open Mic / Rotkreuzplatz.
- 23.4.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 30.4.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 7.5.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 14.5.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 21.5.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 28.5.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 4.6.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 11.6.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 18.6.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 25.6.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 2.7.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 9.7.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 16.7.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 23.7.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
- 30.7.2021 17-19
  Open Mic / angemeldet, Ort wird noch angekündigt
