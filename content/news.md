---
title: "News"
draft: false
---

Aufgrund politischer Maßnahmen verbietet die Stadt unsere Versammlung auf 
dem Marienplatz durchzuführen.

Deswegen findet man uns in nächster Zeit immer am Freitag von 17-19 Uhr an verschiedenen Orten in München.

Stand: 4. April 2021
