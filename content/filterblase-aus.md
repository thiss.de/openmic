---
title: "Filterblase Aus"
date: 2021-03-30T13:06:40+02:00
draft: false
---

# TODO: Wirkweise der Filterblase in digitalen sozialen Netzwerken
# beschreiben und die Bedeutung der Wichtigkeit diese zu
# verlassen, um einen gemeinschaftlichen Standpunkt gegenüber
# der Wirklichkeit einnehmen zu können.

Diskurs im öffentlichen Raum außerhalb der Filterblase digitaler Netzwerke.

Meinungen und Anschauungen in der realen Begegnung und im Miteinander austauschen.
