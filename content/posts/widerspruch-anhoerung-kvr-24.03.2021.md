Sehr geehrte Damen und Herren,

Vielen Dank für die Gelegenheit zur Anhörung.

Hinsichtlich der beabsichtigten Verlegung der von mir angemeldeten Versammlung vom Marienplatz auf den Lenbachplatz komme ich nach Durchsicht der von Ihnen vorgelegten Begründung zu anderen Schlüssen.

Vorausschicken möchte ich, daß die Versammlungsfreiheit nach Art. 8 Grundgesetz "auch ein Selbstbestimmungsrecht über Ort, Zeitpunkt, Art und Inhalt der Veranstaltung [gewährleistet]. Die Bürger sollen damit insbesondere selbst entscheiden können, wo sie ihr Anliegen am wirksamsten zur Geltung bringen können (vgl. BVerfG, Urt. v. 22.02.2011 – 1 BvR 699/06, juris Rn. 63 f.)". (Verwaltungsgericht Kassel, Urt. v. 17.03.2021 - 6 L 562/21.KS)

Die von Ihnen angeführte Begründung verkennt die "verfassungsrechtlichten Vorgaben der Versammlungsfreiheit, die für eine freiheitlich demokratische Staatsordung schlechthin konstituierend ist." (vgl. Verwaltungsgericht Kassel, Urt. v. 17.03.2021 - 6 L 562/21.KS)

Sie verweisen in Ihrer Begründung auf Versammlungen, die am 13.03.2021 in München stattfanden.

Diese hatte jedoch der Art und des Inhaltes nach eine andere Ausrichtung und sind mit der von mir angemeldeten Versammlung nicht vergleichbar.
Der Gleichbehandlung von inhaltlich und der Art anders ausgerichteten, mithin nicht vergleichbaren, Versammlungen mit der von mir angemeldeten verbietet sich dadurch, daß er das Gleichbehandlungsgebot nach Art. 3 Grundgesetz mißachtet. Der Gleichheitssatz verbietet, wesentlich Gleiches ungleich, und gebietet, wesentlich Ungleiches entsprechend seiner Eigenart ungleich zu behandeln. (ständige Rechtsprechung des Bundesverfassungsgerichtes, vgl. u.a. BVerfG, 16. April 2020 - 1 BvR 173/16 -, Rn. 1-46, BVerfG, 04. April 2001 - 2 BvL 7/98 -, Rn. 1-75, BVerfG, 09. März 1994 - 2 BvL 43/92 -, Rn. 1-259)

Da die von mir angemeldete Versammlung offensichtlich sowohl dem Inhalt als der Art nach eine andere Ausrichtung hat, als die Versammlungen, auf die Sie sich beziehen, wird hier Ungleiches auf gleiche Art behandelt. Es dürfte somit evident sein, daß die von Ihnen angeordnete Verlegung des Ortes das Gleichbehandlungsgebot verletzt. 

Die Veranstaltung wird von Herrn Ibing (als Versammlungsleiter eingesetzt) seit 2. Oktober 2020 jede Woche durchgeführt.
In diesem Zeitraum kam es zu keinerlei störenden Vorkommnissen, wie Sie sie beschrieben haben oder einer Überschreitung der angemeldeten Teilnehmerzahl. Insofern ist nicht ersichtlich, auf welchen konkreten und tatsächlichen Anhaltspunkten Sie ihre Progrnose einer Gefahr herleiten.

Auch die am vergangen Freitag von uns durchgeführte Veranstaltung am Karlsplatz verlief störungsfrei und ohne Beanstandung von Seiten der Polizei.

Insbesondere ist auch festzustellen, daß ich keinem Netzwerk von Kritikern der staatlichen Maßnahmen zur Bekämpfung der Corona-Pandemie angehöre oder etwa gut mit einem derartigen Netzwerk verknüpft wäre.

So ist auch anzumerken, daß das von Ihnen genannte Netzwerk von Kritikern staatlicher Maßnahmen zur Bekämpfung der Corona-Pandemie diese Veranstaltung nicht beachtet. Wie Sie sicherlich den Berichten der Polizei zu dem von uns betriebenen Format entnehmen können, bewegt sich die Teilnehmerzahl jeweils im Bereich von deutlich unter 30.

Somit wird deutlich, daß die von mir angemeldete Versammlung keinerlei Anziehungskraft für das von Ihnen angeführte Netzwerk der Kritiker der staatlichen Maßnahmen zur Bekämpfung der Corona-Pandemie hat, somit lassen sich auch keine konkreten und tatsächlichen Anhaltspunkte erkennen, die es rechtfertigen, die grundgesetzlich gewährte Versammlungsfreiheit einzuschränken.

Denn der Grundsatz im Sicherheitsrecht, daß die Schwelle für eine behördliche Beschränkung umso geringer angelegt werden kann, umso höher der zu erwartende Schaden ist, bedarf in jedem Falle einer nachvollziehbaren Begründung. Der Verweis auf eine rein hypothetische Gefahrenlage reicht im allgemeinen nicht aus. Vielmehr bedarf es für die Erstellung einer Gefahrenprognose konkreter und tatsächlicher Anhaltspunkte.

Gerade diese Anhaltspunkte führen sie aber nicht an.

Von daher erwarte ich, daß Sie ihre Entscheidung die von mir angemeldete Versammlung vom Marienplatz auf den Lenbachplatz zu verlegen revidieren und ich wie angemeldet und grundgesetzlich gesichert, die Versammlung auf dem Marienplatz durchführen kann.


Mit freundlichen Grüßen
Thorsten Heimes
